package io.polybius.testtask;

import org.junit.Test;

import static org.junit.Assert.*;

public class MainTest {

    @Test
    public void simpleQuery(){
        assertEquals("[{\"name\":\"Bobby\",\"age\":25}]", Main.getMatchedData("name=Bobby", "[{\"name\":\"Bobby\",\"age\":25},{\"name\":\"Rob\",\"age\":35}]"));
    }
    @Test
    public void differentCase(){
        assertEquals("[{\"name\":\"Bobby\",\"age\":25}]", Main.getMatchedData("name=bOBbY", "[{\"name\":\"Bobby\",\"age\":25},{\"name\":\"Rob\",\"age\":35}]"));
    }
    @Test
    public void queryContainsInString(){
        assertEquals("[{\"name\":\"Robby\",\"age\":35}]", Main.getMatchedData("name=Rob", "[{\"name\":\"Bobby\",\"age\":25},{\"name\":\"Robby\",\"age\":35}]"));
    }


    @Test
    public void emptyQuery(){
        assertEquals("[{\"name\":\"Mike\",\"age\":25},{\"name\":\"Bob\",\"age\":23}]", Main.getMatchedData("", "[{\"name\":\"Mike\",\"age\":25},{\"name\":\"Bob\",\"age\":23}]"));
    }
    @Test
    public void emptyDataSrting(){
        assertEquals("[]", Main.getMatchedData("age>=38 || age>38 || age>=21", ""));
        assertEquals("[]", Main.getMatchedData("name=Jack && height=12.3", "[]"));
    }
    @Test
    public void noInput(){
        assertEquals("[]", Main.getMatchedData("", ""));
    }
    @Test
    public void queryWithSpaces(){
        assertEquals("[{\"name\":\"Bob\",\"age\":25},{\"name\":\"Mike\",\"age\":38}]", Main.getMatchedData("age >= 37 || age <= 27", "[{\"name\":\"Bob\",\"age\":25},{\"name\":\"Mike\",\"age\":38},{\"name\":\"Dean\",\"age\":35},{\"name\":\"John\",\"age\":28}]"));
    }
    @Test
    public void queryWithoutSpaces(){
        assertEquals("[{\"name\":\"Jane\",\"age\":46,\"height\":177.6}]", Main.getMatchedData("height>=176 && age=46", "[{\"name\":\"Jack\",\"height\":164,\"age\":42},{\"name\":\"Jane\",\"age\":46,\"height\":177.6}]"));
    }


    @Test
    public void queryWithOrLogicalOperator(){
        assertEquals("[{\"name\":\"Jack\",\"height\":184.3,\"age\":42},{\"name\":\"Jane\",\"age\":46,\"height\":173.5}]", Main.getMatchedData("height>=176 || age=46", "[{\"name\":\"Jack\",\"height\":184.3,\"age\":42},{\"name\":\"Jane\",\"age\":46,\"height\":173.5},{\"name\":\"Paul\",\"age\":35,\"height\":170.5}]"));
    }
    @Test
    public void queryWithAndLogicalOperator(){
        assertEquals("[{\"name\":\"Mark\",\"weight\":78.3,\"height\":178.8}]", Main.getMatchedData("weight>60&&height>170", "[{\"name\":\"Jane\",\"height\":160},{\"name\":\"Mark\",\"weight\":78.3, \"height\":178.8}]"));
    }
    @Test
    public void queryWithBothOperators(){
        assertEquals("[{\"name\":\"Robert\",\"age\":36,\"weight\":93.31,\"height\":176.71}]", Main.getMatchedData("age>40 || name=Lucy && weight<=45.6 || height>170.1", "[{\"name\":\"Bob\",\"age\":35,\"weight\":75.74,\"height\":170},{\"name\":\"Robert\",\"age\":36,\"weight\":93.31,\"height\":176.71}]"));
    }



    @Test
    public void integerWithEqual(){
        assertEquals("[{\"name\":\"Dean\",\"age\":30}]", Main.getMatchedData("age=30", "[{\"name\":\"John\",\"age\":26},{\"name\":\"Dean\",\"age\":37},{\"name\":\"Mike\",\"age\":22},{\"name\":\"Lucy\",\"age\":24},{\"name\":\"Dean\",\"age\":30}]"));
    }
    @Test
    public void integerWithGreaterOrEqual(){
        assertEquals("[{\"name\":\"Jane\",\"age\":35},{\"name\":\"Bob\",\"age\":42}]", Main.getMatchedData("age>=35", "[{\"name\":\"John\",\"age\":28},{\"name\":\"Mike\",\"age\":32},{\"name\":\"Jane\",\"age\":35},{\"name\":\"Bob\",\"age\":42}]"));
    }
    @Test
    public void integerWithLessOrEqual(){
        assertEquals("[{\"name\":\"Lucy\",\"age\":23},{\"name\":\"Robert\",\"age\":25},{\"name\":\"Robert\",\"age\":24}]", Main.getMatchedData("age<=25", "[{\"name\":\"Lucy\",\"age\":23},{\"name\":\"Robert\",\"age\":25},{\"name\":\"Mike\",\"age\":28},{\"name\":\"Robert\",\"age\":24}]"));
    }
    @Test
    public void integerWithGreater(){
        assertEquals("[{\"name\":\"Bob\",\"age\":36}]", Main.getMatchedData("age>35", "[{\"name\":\"Sam\",\"age\":22},{\"name\":\"Bob\",\"age\":36},{\"name\":\"Jane\",\"age\":35},{\"name\":\"Mike\",\"age\":24}]"));
    }
    @Test
    public void integerWithLess(){
        assertEquals("[{\"name\":\"Jane\",\"age\":29},{\"name\":\"Jane\",\"age\":27}]", Main.getMatchedData("age<30", "[{\"name\":\"Jane\",\"age\":29},{\"name\":\"Dean\",\"age\":34},{\"name\":\"Jane\",\"age\":27}]"));
    }

    @Test
    public void decimalWithEqual(){
        assertEquals("[{\"name\":\"Robert\",\"height\":179.3}]", Main.getMatchedData("name=Robert && height=179.3","[{\"name\":\"Robert\",\"height\":179.3},{\"name\":\"Mike\",\"height\":179.4}]"));
    }
    @Test
    public void decimalWithGreaterOrEqual(){
        assertEquals("[{\"name\":\"Jane\",\"height\":178.45},{\"name\":\"Taylor\",\"height\":177.65}]", Main.getMatchedData("height>=177.65", "[{\"name\":\"Alice\",\"height\":163.04},{\"name\":\"Bob\",\"height\":174.34},{\"name\":\"Jane\",\"height\":178.45},{\"name\":\"Taylor\",\"height\":177.65}]"));
    }
    @Test
    public void decimalWithLessOrEqual(){
        assertEquals("[{\"name\":\"Lucy\",\"height\":175.31},{\"name\":\"Dean\",\"height\":177.34}]", Main.getMatchedData("height<=177.34", "[{\"name\":\"Lucy\",\"height\":175.31},{\"name\":\"Dean\",\"height\":177.34},{\"name\":\"Mike\",\"height\":179.79},{\"name\":\"John\",\"height\":178.91}]"));
    }
    @Test
    public void decimalWithGreater(){
        assertEquals("[{\"name\":\"Jane\",\"height\":173.48},{\"name\":\"Mike\",\"height\":179.61},{\"name\":\"Mike\",\"height\":176.02}]", Main.getMatchedData("height>170.01", "[{\"name\":\"Jane\",\"height\":173.48},{\"name\":\"Mike\",\"height\":179.61},{\"name\":\"Taylor\",\"height\":169.42},{\"name\":\"Mike\",\"height\":176.02}]"));
    }
    @Test
    public void decimalWithLess(){
        assertEquals("[{\"name\":\"Dean\",\"height\":164.05}]", Main.getMatchedData("height<165.555", "[{\"name\":\"Sam\",\"height\":176.75},{\"name\":\"Dean\",\"height\":164.05},{\"name\":\"Alice\",\"height\":173.48},{\"name\":\"Mike\",\"height\":173.23}]"));
    }


}