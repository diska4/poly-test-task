package io.polybius.testtask;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
  public static void main(String[] args){
    String query = args[0];
    String dataString = args[1];
    System.out.println(getMatchedData(query, dataString));
  }

  public static String getMatchedData(String query, String dataString){
      if (dataString.equals("[]") || dataString.equals(""))return "[]";
      if (query.equals(""))return dataString;

      List<String> queryPartsList = Arrays.asList(query.replaceAll(" ", "").split("\\|\\|"));

      JsonArray jsonArray = new JsonParser().parse(dataString).getAsJsonArray();
      List<JsonObject> filteredResult = new ArrayList<>();
      mainloop:
      for (int i = 0; i < jsonArray.size(); i++) {

          JsonObject object = jsonArray.get(i).getAsJsonObject();
          loop:
          for(String andString : queryPartsList){
            for (String s : andString.split("&&")) {
                if (!parseQueryPart(s, object))continue loop;
            }
            filteredResult.add(object);
            continue mainloop;

        }
      }
      return new Gson().toJson(filteredResult);

  }

  private static boolean parseQueryPart(String queryPart, JsonObject object){

    String key;
    double number;

    if (queryPart.contains(">=")){
      key = queryPart.split(">=")[0];
      number = Double.parseDouble(queryPart.split(">=")[1]);
      return object.get(key) != null && object.get(key).getAsDouble() >= number;
    }

    if (queryPart.contains("<=")){
      key = queryPart.split("<=")[0];
      number = Double.parseDouble(queryPart.split("<=")[1]);
      return object.get(key) != null && object.get(key).getAsDouble() <= number;
    }

    if (queryPart.contains(">")){
      key = queryPart.split(">")[0];
      number = Double.parseDouble(queryPart.split(">")[1]);
      return object.get(key) != null && object.get(key).getAsDouble() > number;
    }

    if (queryPart.contains("<")){
      key = queryPart.split("<")[0];
      number = Double.parseDouble(queryPart.split("<")[1]);
      return object.get(key) != null && object.get(key).getAsDouble() < number;
    }

    if (queryPart.contains("=")){
      key = queryPart.split("=")[0];
      String string = queryPart.split("=")[1];
      try{
        double numeric = Double.parseDouble(string);
        return object.get(key) != null && object.get(key).getAsDouble() == numeric;
      }catch (NumberFormatException e){
      }
      return object.get(key) != null && object.get(key).getAsString().toLowerCase().contains(string.toLowerCase());
    }
    return false;
  }

}
